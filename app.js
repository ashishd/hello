var http      = require('http');
// var mongoose  = require('mongoose');
var express   = require('express');



// Body parser is used to get value from req.body
var bodyParser = require('body-parser');

// var multer = require('multer');
// To use mongo db
// var mongoose = require('mongoose');
// var cookieParser = require('cookie-parser');
// var session = require('express-session');
// var multipart = require('connect-multiparty');

var app    = express();
var db;

console.log('starting the Express (NodeJS) Web server');
app.use(bodyParser.text());
app.use(bodyParser.urlencoded({limit: '50mb', extended: false}));
// app.use(express.bodyParser());
// app.use(multer());

// app.use(cookieParser());
// app.use(secretSession);
var PORT = process.env.PORT || 3000;
app.set('port', PORT);
app.listen(PORT, function() {
  console.log('Webserver is listening on port 3000');
  console.log('Node app is running on port', app.get('port'));
});

// var server = app.listen(port);
app.get('/', function(req, res){
  res.send("Hello");
});

app.post('/', function(req, res) {
  var data = req.body;
  res.send(data);
});

app.use(function(err, req, res, next){
  if (req.xhr) {
    res.send(500, 'Something went wrong!');
  }
  else {
    next(err);
  }
});



// var config = {
//       "USER"    : "",
//       "PASS"    : "",
//       "HOST"    : "ec2-xx-xx-xx-xx.ap-southeast-2.compute.amazonaws.com",
//       "PORT"    : "27017",
//       "DATABASE" : "my_example"
//     };

// var dbPath  = "mongodb://"+config.USER + ":"+
//     config.PASS + "@"+
//     config.HOST + ":"+
//     config.PORT + "/"+
//     config.DATABASE;
var standardGreeting = 'Hello World!';